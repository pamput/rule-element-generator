import { contents, simpleRules } from "./components.js";
import { capitalize, deCapitalize } from "./utils.js";
import wiki from "./wiki.js";

function delay(delayInms) {
    return new Promise(resolve => {
        setTimeout(() => {
            resolve(2);
        }, delayInms);
    });
}


export default class RuleElementGenerator extends Application {

    constructor(options) {
        super(options);
        this.DATA = options.data; // Rule Element to be edited
        this.SOURCE = options.source; // Context or index to be updated.
        this.NEW = options.new; // Whether a new Rule Element is being created

        this.PrepareFields(); // Has delays because it seems like they run before it's ready otherwise.
    }

    async PrepareFields() {
        await delay(100);
        this.Initialize();
        await delay(100);
        this.Update();
    }

    activateListeners($html) {
        super.activateListeners($html);

        // Update the fields shown when the type of rule element changes
        $html
            .find("#reKey")
            .change(() => {
                if($("#reKey").val() !== JSON.parse($("#reRuleElement").text()).key) $("#reContent").html(contents[$("#reKey").val()]);
            });

        // Update the preview
        $html
            .find("#reUpdate")
            .click(this.Update);

        // Wiki Information
        $html
            .find("#reInfo")
            .click(() => {
                let content = `<p>Learn more about rule elements <a href="https://github.com/foundryvtt/pf2e/wiki/Quickstart-guide-for-rule-elements">here</a></p>`;
                content += wiki[$("#reKey").val()];
                new Dialog({title: "Info", content, buttons: {}, }, {width: 600, }).render(true);
            });
        
        // Apply the changes
        $html
            .find("#reApply")
            .click(() => {
                if (this.NEW) {
                    const currentRules = this.SOURCE.item.data.data.rules;
                    this.SOURCE.item.update({
                        "data.rules": currentRules.concat(JSON.parse($("#reRuleElement").val())),
                    });
                } else {
                    $(`textarea[name="${this.SOURCE}"]`).text($("#reRuleElement").val()).trigger("change");
                }
                ui.notifications.info("Rule Element Generator | Applied");
                this.close();
            });
    }

    // Spaghetti clump #1: Runs at start
    Initialize() { 
        $("#reKey").val(this.DATA.key);
        $("#reContent").html(contents[$("#reKey").val()]);

        //most input handling
        for (let x of Object.keys(this.DATA).filter((value) =>
            [
                "label",
                "type",
                "mode",
                "path",
                "text",
                "value",
                "diceNumber",
                "dieSize",
                "damageType",
                "critical",
                "property",
                "scope",
                "acuity",
                "range",
                "category",
                "group",
                "priority",
                "except",
                "domain",
                "option",
                "targetId",
                "uuid",
                "prompt",
                "replaceSelf",
                "adjustName",
                "flag",
                "alternate",
                "keep",
            ].includes(value)
        )) {
            if ($("#re" + capitalize(x))) {
                $("#re" + capitalize(x)).val(this.DATA[x].toString());
            }
        }

        //various list handling
        if (this.DATA.rollOptions) $("#reRollOptions").val(this.DATA.rollOptions.toString());
        if (this.DATA.add) $("#reAdd").val(this.DATA.add.toString());
        if (this.DATA.traits) $("#reTraits").val(this.DATA.traits.toString());

        //advanced selector handling
        if (this.DATA.selector) {
            if (this.DATA.selector.includes("group")) {
                $("#reAdvSelector").val(this.DATA.selector.split("group-")[0] + "group-");
                $("#reSelector").val(this.DATA.selector.split("group-")[1]);
            } else if (this.DATA.selector.includes("}")) {
                $("#reAdvSelector").val(this.DATA.selector.split("}-")[0] + "}-");
                $("#reSelector").val(this.DATA.selector.split("}-")[1]);
            } else {
                $("#reSelector").val(this.DATA.selector);
            }
        }
            
        //precision and override handling
        if (this.DATA.category === "precision") $("#reDamageType").val("precision");

        if ($("#reDieSize")) {
            $("#reDieSize").val(this.DATA.dieSize);
        }
        if ($("#reDiceNumber")) {
            $("#reDiceNumber").val(this.DATA.diceNumber);
        }

        if (this.DATA.override) {
            if ($("#reDieSize")) {
                $("#reDieSize").val(this.DATA.override.dieSize);
            }
            if ($("#reDiceNumber")) {
                $("#reDiceNumber").val(this.DATA.override.diceNumber);
            }
            if ($("#reOverride")) {
                $("#reOverride").prop("checked", true);
            }
        }

        //array object handling
        if (this.DATA.predicate) {
            if (this.DATA.predicate.all) $("#reAll").val(this.DATA.predicate.all.toString());
            if (this.DATA.predicate.any) $("#reAny").val(this.DATA.predicate.any.toString());
            if (this.DATA.predicate.not) $("#reNot").val(this.DATA.predicate.not.toString());
        }
        if (this.DATA.definition) {
            if (this.DATA.definition.all) $("#reDefAll").val(this.DATA.definition.all.toString());
            if (this.DATA.definition.any) $("#reDefAny").val(this.DATA.definition.any.toString());
            if (this.DATA.definition.not) $("#reDefNot").val(this.DATA.definition.not.toString());
        }
        if (this.DATA.allowedDrops) {
            if (this.DATA.allowedDrops.all) $("#reADAll").val(this.DATA.allowedDrops.all.toString());
            if (this.DATA.allowedDrops.any) $("#reADAny").val(this.DATA.allowedDrops.any.toString());
            if (this.DATA.allowedDrops.not) $("#reADNot").val(this.DATA.allowedDrops.not.toString());
            if (this.DATA.allowedDrops.label) $("#reADLabel").val(this.DATA.allowedDrops.label);
        }

        if (this.DATA.value?.bright) {
            for (let x of Object.keys(this.DATA.value)) {
                $("#re" + capitalize(x)).val(this.DATA.value[x].toString());
            }
        }
        if (this.DATA.value?.animation) {
            for (let x of Object.keys(this.DATA.value.animation)) {
                $("#re" + capitalize(x)).val(this.DATA.value.animation[x].toString());
            }
        }

        //adjustment handling
        if (this.DATA.adjustment) {
            if (this.DATA.adjustment.criticalSuccess) $("#reCrit").val(this.DATA.adjustment.criticalSuccess);
            if (this.DATA.adjustment.success) $("#reSuccess").val(this.DATA.adjustment.success);
            if (this.DATA.adjustment.failure) $("#reFail").val(this.DATA.adjustment.failure);
            if (this.DATA.adjustment.criticalFailure) $("#reFumble").val(this.DATA.adjustment.criticalFailure);
        }

        //outcome handling
        if (this.DATA.outcome) {
            if (this.DATA.outcome.includes("criticalSuccess")) $("#reOutcomeCrit").prop("checked", true);
            if (this.DATA.outcome.includes("success")) $("#reOutcomeSuccess").prop("checked", true);
            if (this.DATA.outcome.includes("failure")) $("#reOutcomeFail").prop("checked", true);
            if (this.DATA.outcome.includes("criticalFailure")) $("#reOutcomeFumble").prop("checked", true);
        }

        //damage handling
        if (this.DATA.key === "Strike" && this.DATA.damage?.base) {
            if (this.DATA.damage.base.dice || this.DATA.damage.base.die || this.DATA.damage.base.damageType) {
                if (this.DATA.damage.base.dice) $("#reDice").val(this.DATA.damage.base.dice.toString());
                if (this.DATA.damage.base.die) $("#reDie").val(this.DATA.damage.base.die.toString());
                if (this.DATA.damage.base.damageType) $("#reDamage").val(this.DATA.damage.base.damageType.toString());
            }
        }

        //brackets handling
        if (this.DATA.value?.brackets) {
            $("#reValue").val("Overridden by Brackets");

            let temp_list = [];
            let isDiceNum = 0;
            for (let i = 0; true; i++) {
                if (!this.DATA.value.brackets[i]) break;

                temp_list.push(this.DATA.value.brackets[i].start);
                if (this.DATA.value.brackets[i].value?.diceNumber) {
                    temp_list.push(this.DATA.value.brackets[i].value.diceNumber);
                    isDiceNum = 1;
                } else {
                    temp_list.push(this.DATA.value.brackets[i].value);
                }
                temp_list.push(this.DATA.value.brackets[i].end);
            }

            if (isDiceNum) $("#reBracketDiceNumber").prop("checked", true);

            if (this.DATA.value.field) $("#reField").val(this.DATA.value.field);

            $("#reBrackets").val(temp_list.toString());
        }

        //choices handling
        if (this.DATA.choices) {
            let temp_list = [];
            for (let i = 0; true; i++) {
                if (!this.DATA.choices[i]) break;

                temp_list.push(this.DATA.choices[i].label);
                temp_list.push(this.DATA.choices[i].value);
            }

            $("#reChoices").val(temp_list.toString());
        }
    }

    // Spaghetti clump #2: Runs whenever a change is made in the left column
    Update() {
        let data = {};
        for (let x of ["key", ...simpleRules]) {
            //test for properties (after capitalizing the first letter capitalized and prepending "re" to match up with ids)
            let propertyValue = $("#re" + capitalize(x))?.val();
            if (propertyValue) {
                //makes sure numbers are saved as such
                if (parseInt(propertyValue) && !["label", "prompt", "targetId", "uuid"].includes(x)) {
                    data[deCapitalize(x)] = parseInt(propertyValue);
                } else if (propertyValue === "true" || propertyValue === "false") {
                    if (propertyValue === "true") data[x] = true;
                    if (propertyValue === "false") data[x] = false;
                } else {
                    data[deCapitalize(x)] = propertyValue;
                }
            }
        }

        if ($("#reRollOptions").val()) data["roll-options"] = $("#reRollOptions").val().split(",");
        if ($("#reAdd").val()) data.add = $("#reAdd").val().split(",");
        if ($("#reTraits").val()) data.traits = $("#reTraits").val().split(",");

        if ($("#reAdvSelector").val()) {
            data.selector = data.advSelector + data.selector;
            delete data.advSelector;
        }

        if ($("#reDamageType").val() == "precision") {
            data.category = data.damageType;
            delete data.damageType;
        }

        if ($("#reDieSize").val()) data.dieSize = $("#reDieSize").val();
        if ($("#reDiceNumber").val()) data.diceNumber = $("#reDiceNumber").val();

        if ($("#reOverride").is(":checked")) {
            data.override = {
                diceNumber: data.diceNumber,
                dieSize: data.dieSize
            };
            delete data.dieSize;
            delete data.diceNumber;
        }

        if ($("#reCrit").val() || $("#reSuccess").val() || $("#reFail").val() || $("#reFumble").val()) {
            data.adjustment = {};
            if ($("#reCrit").val()) data.adjustment.criticalSuccess = $("#reCrit").val();
            if ($("#reSuccess").val()) data.adjustment.success = $("#reSuccess").val();
            if ($("#reFail").val()) data.adjustment.failure = $("#reFail").val();
            if ($("#reFumble").val()) data.adjustment.criticalFailure = $("#reFumble").val();
        }

        if ($("#reBright").val() || $("#reDim").val() || $("#reColor").val() || $("#reShadows").val()) {
            data.value = {};
            if ($("#reBright").val()) data.value.bright = $("#reBright").val();
            if ($("#reDim").val()) data.value.dim = $("#reDim").val();
            if ($("#reColor").val()) data.value.color = $("#reColor").val();
            if ($("#reShadows").val()) data.value.shadows = $("#reShadows").val();

            if ($("#reIntensity").val() || $("#reSpeed").val() ||$("$reType").val()) {
                data.value.animation = {};
                if ($("#reIntensity").val()) data.value.animation.intensity = $("#reIntensity").val();
                if ($("#reSpeed").val()) data.value.animation.speed = $("#reSpeed").val();
                if ($("#reType").val()) data.value.animation.type = $("#reType").val();
            }
        }

        if (
            $("#reOutcomeCrit").is(":checked") ||
            $("#reOutcomeSuccess").is(":checked") ||
            $("#reOutcomeFail").is(":checked") ||
            $("#reOutcomeFumble").is(":checked")
        ) {
            data.outcome = [];
            if ($("#reOutcomeCrit").is(":checked")) data.outcome.push("criticalSuccess");
            if ($("#reOutcomeSuccess").is(":checked")) data.outcome.push("success");
            if ($("#reOutcomeFail").is(":checked")) data.outcome.push("failure");
            if ($("#reOutcomeFumble").is(":checked")) data.outcome.push("criticalFailure");
        }

        if ($("#reDice").val() || $("#reDie").val() || $("#reDamage").val()) {
            data.damage = { base: {} };
            if ($("#reDice").val()) data.damage.base.dice = parseInt($("#reDice").val());
            if ($("#reDie").val()) data.damage.base.die = $("#reDie").val();
            if ($("#reDamage").val()) data.damage.base.damageType = $("#reDamage").val();
        }

        if ($("#reAll").val() || $("#reAny").val() || $("#reNot").val()) {
            data.predicate = {};
            if ($("#reAll").val()) data.predicate.all = $("#reAll").val().split(",");
            if ($("#reAny").val()) data.predicate.any = $("#reAny").val().split(",");
            if ($("#reNot").val()) data.predicate.not = $("#reNot").val().split(",");
        }

        if ($("#reDefAll").val() || $("#reDefAny").val() || $("#reDefNot").val()) {
            data.definition = {};
            if ($("#reDefAll").val()) data.definition.all = $("#reDefAll").val().split(",");
            if ($("#reDefAny").val()) data.definition.any = $("#reDefAny").val().split(",");
            if ($("#reDefNot").val()) data.definition.not = $("#reDefNot").val().split(",");
        }

        if ($("#reADLabel").val() || $("#reADAll").val() || $("#reADAny").val() || $("#reADNot").val()) {
            data.allowedDrops = {};
            if ($("#reADLabel").val()) data.allowedDrops.label = $("#reADLabel").val().toString();
            if ($("#reADAll").val()) data.allowedDrops.all = $("#reADAll").val().split(",");
            if ($("#reADAny").val()) data.allowedDrops.any = $("#reADAny").val().split(",");
            if ($("#reADNot").val()) data.allowedDrops.not = $("#reADNot").val().split(",");
        }

        // brackets handling
        if ($("#reBrackets").val()) {
            if ($("#reBracketDiceNumber").is(":checked")) delete data.diceNumber;

            data.value = { brackets: [] };
            if ($("#reField").val()) data.value["field"] = $("#reField").val();

            let vals = $("#reBrackets").val().split(",");
            let starts = [];
            let ends = [];
            let values = [];
            for (let i = 0; i < vals.length; i++) {
                if (i % 3 == 0) starts.push(vals[i]);
                if (i % 3 == 1) values.push(vals[i]);
                if (i % 3 == 2) ends.push(vals[i]);
            }
            for (let i = 0; i < Math.max(starts.length, ends.length, values.length); i++) {
                data.value.brackets.push({});
                if (starts[i]) Object.assign(data.value.brackets[i], { start: parseInt(starts[i]) });
                if ($("#reBracketDiceNumber").is(":checked")) {
                    if (values[i]) Object.assign(data.value.brackets[i], { value: { diceNumber: parseInt(values[i]) } });
                } else {
                    if (values[i]) Object.assign(data.value.brackets[i], { value: parseInt(values[i]) });
                }
                if(ends[i]) Object.assign(data.value.brackets[i], { end: parseInt(ends[i]) });
            }
        }

        if ($("#reChoices").val()) {
            data.choices = [];
            let vals = $("#reChoices").val().split(",");
            let labels = [];
            let values = [];
            for (let i = 0; i < vals.length; i++) {
                if (i % 2 == 0) labels.push(vals[i]);
                if (i % 2 == 1) values.push(vals[i]);
            }
            for (let i = 0; i < Math.max(labels.length, values.length); i++) {
                data.choices.push({});
                if (labels[i]) Object.assign(data.choices[i], { label: labels[i].toString() });
                if (values[i]) Object.assign(data.choices[i], { value: values[i] });
            }
        }

        // Update the preview to reflect the field inputs
        $("#reRuleElement").html(JSON.stringify(data, undefined, 2));
    }

}