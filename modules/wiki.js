const wiki = {
FlatModifier: `
<h3>My First Rule Element</h3>
<p>The first Rule Element for this guide is the Item Bonus of the Armbands of Athleticism</p>
<code>
{
    "key": "FlatModifier",
    "label": "Armbands of Athleticism",
    "selector": "athletics",
    "slug": "armbands-of-athleticism-1",
    "type": "item",
    "value": 2
}
</code>
<p>They might be tricky to get the hang of.  These once set up, will save you lines and lines of code.</p>
<p>Let's break these down:</p>
<ol>
    <li>Key: This is the name given to the rule element within the code.
        <ul>
            <li>If this is not correct your rule with do nothing!</li>
        </ul>
    </li>
    <li>Label: Name your rule. Some rule elements will use this for displaying in the UI: e.g., <code>FlatModifier</code>, which will use the label for the modifier appearing in roll cards. If a label is omitted, the name of the rule element's parent item is used instead.
    </li>
    <li>Selector: Specifies, or selects, what to apply your modifier to.
        <ul>
            <li>There is a list at the bottom of the document.</li>
            <li>An incorrect selector will make your rule do nothing</li>
            <li>Add the _id/ of an item to the selector to target a specific item or item type</li>
        </ul>
    </li>
    <li>Slug: If more than one rule element has the same selector and label, all but the first will be dropped. A slug allows one to work around this limitation. It is not usually needed, however, since rarely will the selector/label combination have any collisions to worry about.
    </li>
    <li>Type: This is the modifier type,
        <ul>
            <li>With this set appropriately the modifier will be taken into account for bonus stacking rules</li>
        </ul>
    </li>
    <li>Value: the value can be minus if you are applying a penalty</li>
</ol>
<p>A slightly more advanced case appears in inspire courage, only one of these contains the new element, so this guide will keep the other two "minified"</p>
<code>
{"key":"FlatModifier","label":"Inspire Courage","selector":"attack","type":"status","value":1}

{"key":"FlatModifier","label":"Inspire Courage","selector":"damage","type":"status","value":1}

{
    "key":"FlatModifier",
    "label":"Inspire Courage (vs fear)",
    "predicate":{
        "all":[
            "fear"
        ]
    },
    "selector":"saving-throw",
    "type":"status",
    "value":1
}
</code>
<ol start="7">
    <li>Predicate: If you want your bonus to apply only at certain times, like only vs. fear, you can use a predicate, which comes in three modes:
        <ul>
            <li>"all": if everything in the list is present for a roll the modifier is applied</li>
            <li>"any": if at least one in the list is present the modifier is applied</li>
            <li>"not": if at least one item in the list is present the modifier is not applied</li>
            <li>an incorrect predicate will make your rule act unexpectedly, these are case sensitive.</li>
            <li>predicates currently enjoy automation from the spell save button and weapon attacks/damage by querying all their traits. They can also be set in Macros.</li>
        </ul>
    </li>
</ol>
`,
Weakness: `
<h3>Immunity, Weakness, Resistance</h3>
<p>You can add immunity, weakness, resistance (IWR) to a creature with rule elements.  The automation for application of damage is not complete yet but this rule element lets you easily track your IWR.  Using any of them is quite straightforward.</p>
<p>example (fire weakness)</p>
<code>{ "key": "Weakness", "type": "fire", "value": 5 }</code>
<p>But they can also be handed roll syntax to evaluate the value.</p>
<p>example (negative resist equal to half level minimum 1)</p>
<code>{ "key": "Resistance", "type": "negative", "value": "max(1,floor(@details.level.value/2))" }</code>
<p>Immunity can be to a damage type or a condition.</p>
<p>example (bleed immunity)</p>
<code>{ "key": "Immunity", "type": "bleed" }</code>
<p>You can also set exceptions.</p>
<p>example (resist all except force)</p>
<code>{ "key": "Resistance", "type": "all", "value": 5, "except": "force" }</code>
<p>You cannot set the type to a damage type or condition that does not exist however, if you added a resistance to "holy" damage the resistance would not appear, but changing the type to "good" would allow it to show up.  Unique resistances, such as resistance 10 to ranged weapons, need to be handled as unique abilities instead.</p>
`,
Resistance: `
<h3>Immunity, Weakness, Resistance</h3>
<p>You can add immunity, weakness, resistance (IWR) to a creature with rule elements.  The automation for application of damage is not complete yet but this rule element lets you easily track your IWR.  Using any of them is quite straightforward.</p>
<p>example (fire weakness)</p>
<code>{ "key": "Weakness", "type": "fire", "value": 5 }</code>
<p>But they can also be handed roll syntax to evaluate the value.</p>
<p>example (negative resist equal to half level minimum 1)</p>
<code>{ "key": "Resistance", "type": "negative", "value": "max(1,floor(@details.level.value/2))" }</code>
<p>Immunity can be to a damage type or a condition.</p>
<p>example (bleed immunity)</p>
<code>{ "key": "Immunity", "type": "bleed" }</code>
<p>You can also set exceptions.</p>
<p>example (resist all except force)</p>
<code>{ "key": "Resistance", "type": "all", "value": 5, "except": "force" }</code>
<p>You cannot set the type to a damage type or condition that does not exist however, if you added a resistance to "holy" damage the resistance would not appear, but changing the type to "good" would allow it to show up.  Unique resistances, such as resistance 10 to ranged weapons, need to be handled as unique abilities instead.</p>
`,
Immunity: `
<h3>Immunity, Weakness, Resistance</h3>
<p>You can add immunity, weakness, resistance (IWR) to a creature with rule elements.  The automation for application of damage is not complete yet but this rule element lets you easily track your IWR.  Using any of them is quite straightforward.</p>
<p>example (fire weakness)</p>
<code>{ "key": "Weakness", "type": "fire", "value": 5 }</code>
<p>But they can also be handed roll syntax to evaluate the value.</p>
<p>example (negative resist equal to half level minimum 1)</p>
<code>{ "key": "Resistance", "type": "negative", "value": "max(1,floor(@details.level.value/2))" }</code>
<p>Immunity can be to a damage type or a condition.</p>
<p>example (bleed immunity)</p>
<code>{ "key": "Immunity", "type": "bleed" }</code>
<p>You can also set exceptions.</p>
<p>example (resist all except force)</p>
<code>{ "key": "Resistance", "type": "all", "value": 5, "except": "force" }</code>
<p>You cannot set the type to a damage type or condition that does not exist however, if you added a resistance to "holy" damage the resistance would not appear, but changing the type to "good" would allow it to show up.  Unique resistances, such as resistance 10 to ranged weapons, need to be handled as unique abilities instead.</p>
`,
FastHealing: `
<h3>Fast Healing</h3>
<p>The FastHealing rule element can provide a reminder that you have fast healing active on the character.</p>
<p>LifeBoost</p>
<code>
{
    "key": "FastHealing",
    "value": "@item.level * 2"
}
</code>
`,
DamageDice: `
<h3>Damage Dice</h3>
<p>A Damage Dice Rule element adds additional Dice to damage or critical damage rolls. The example adds 3d6 piercing damage to a critical strike with an improvised weapon.</p>
<p>DamageDice</p>
<code>
{
    "critical": true,
    "key": "DamageDice",
    "selector": "damage",
    "diceNumber": 3,
    "dieSize": "d6",
    "damageType": "piercing",
    "predicate": {
        "all": [
            "improvised"
        ]
    },
    "label": "Shattering Strike"
}
</code>
<p>dieSize and damageType can be omitted to add additional damage dice of the type and size that the weapon already has to the damage.</p>
<p>The diceNumber or dieSize properties can be set in a value brackets object, examples for such use are in the advanced section. Targeting a very specific weapon with this rule is also explained in the advanced section.</p>
<p>The DamageDice rule element can also pass traits to the roll</p>
<code>
{
    "key": "DamageDice",
    "selector": "jaws-damage",
    "traits": ["poison"]
}
</code>
<p>These can be used regardless of whether dice are added or not. However, these traits are not "functional" in the way normally added traits are. So adding <code>deadly-d8</code> would show on the chat card but would not be factored into the damage on a critical. Since they only add on damage they also cannot be used to trigger parts of the attack roll, for example a weapon being given the poison trait vs an NPC with a bonus to AC vs poison would not be automated using this. A more comprehensive and functional trait adding rule element will be added later.</p>
`,
BaseSpeed: `
<h3>Base Speed</h3>
<p>To add a base speed value, maybe with a Fly Spell Effect or with a feat granting a climbing speed a Base Speed Rule can be used.</p>
<p>The speed rule elements were built for the familiars, with the intention of refactoring the PCs to use the same data structure for speed. That refactoring has not (yet?) happened.</p>
<p>BaseSpeed</p>
<code>{ "key": "BaseSpeed", "selector": "fly", "value": 30 }</code>
<p>The selector for the Base Speed Element differs from the list of selectors at the end of this file by omitting the "-speed" part.</p>
`,
FixedProficiency: `
<h3>Fixed Proficiency</h3>
<p>This Rule Element can be used well for Animal Form and similar spells, as the name suggests it sets the proficiency to the given value. It appears in the game as a modifier to the skill that makes up for the difference.</p>
<p>FixedProficiency</p>
<code>{ "key": "FixedProficiency", "selector": "athletics", "value": 9 }</code>
<p></p>
`,
Strike: `
<h3>Strike</h3>
<p>This rule element is a bit trickier in that there are a lot of necessary fields. The easiest way to create your own custom strike would be to start from a finished strike like the monk's Tiger Claw strike here.</p>
<p>Strike</p>
<code>
{
    "key": "Strike",
    "category": "unarmed",
    "damage": {
        "base": {
            "damageType": "slashing",
            "dice": 1,
            "die": "d8"
        }
    },
    "group": "brawling",
    "label": "Tiger Claw",
    "traits": [
        "agile",
        "finesse",
        "unarmed",
        "nonlethal"
    ]
}
</code>
<p>If necessary an "ability": "int"-entry could be used for a strike that scales its to-hit with intelligence, as the "Spiritual Weapon" Spell could. This works with the other 3 character ability shorthands too. To add a Flat part to the damage like an ability modifier, add an additional Flat Modifier Element.  An optional <code>"range": 30</code> parameter can be given to denote the range of an attack.  This only accepts a number or <code>null</code>. It no longer needs to specify melee.</p>
`,
Note: `
<h3>Note</h3>
<p>The Note Rule Element adds additional text to the chat output of a roll. The Example (taken from the Alchemist's Evasion Feat) adds a reminder to all reflex saves about the feat.</p>
<p>Note</p>
<code>{ "key":"Note", "selector":"reflex", "text":"&ltp class='compact-text'&gt&ltstrong&gt{item|name}&lt/strong&gt When you roll a success on a Reflex save, you get a critical success instead.&lt/p&gt" }</code>
<p>for damage Rolls, it is possible to set a message only on a critical roll or only on a normal hit.</p>
<code>{ "key": "Note", "selector": "damage", "text": "&ltp class='compact-text'&gt&ltstrong&gtAxiomatic Rune&lt/strong&gt When you critically succeed at an attack roll with this weapon against a chaotic creature, instead of rolling, count each weapon damage die as average damage rounded up (3 for d4, 4 for d6, 5 for d8, 6 for d10, 7 for d12).&lt/p&gt", "outcome": ["criticalSuccess"] }</code>
`,
DexterityModifierCap: `
<h3>Dexterity Modifier Cap</h3>
<p>Some spells and items, while not armor, do impose a cap on the dexterity modifier. For those, a Dexterity Modifier Cap can be set per Rule Element.</p>
<p>DexterityModifierCap</p>
<code>{ "key": "DexterityModifierCap", "value": 5 }</code>
`,
Sense: `
<h3>Sense</h3>
<p>A Rule Element for Feats, Spells, and Items that grant additional senses or increase the acuity of existing ones. The Examples are the bloodhound mask and the Sensate Gnomes scent.</p>
<p>Sense</p>
<code>
{
    "key": "Sense",
    "selector": "darkvision"
}
</code>
<code>
{
    "acuity": "imprecise",
    "key": "Sense",
    "range": 30,
    "selector": "scent"
}
</code>
<p>For these Rules, the Label is queried from the language database.</p>
`,
WeaponPotency: `
<h3>Weapon Potency and Striking</h3>
<p>The weapon potency and striking rule elements do basically the same as the flat modifier and damage dice rule elements, but with the additional feature of properly changing the damage rolls for things like backstabber (+2 to damage with a +3 potency rune) and deadly (more dice if striking rune).</p>
<p>WeaponPotency</p>
<code>{ "key": "WeaponPotency", "selector": "{item|_id}-attack", "value": 1 }</code>
<p>Striking</p>
<code>{ "key": "Striking", "selector": "{item|_id}-damage", "value": 1 }</code>
<p>Another Example would be the Handwraps of mighty blows at +1 striking:</p>
<code>{ "key": "WeaponPotency", "predicate":{ "all":[ "unarmed" ] }, "selector": "attack", "value": 1 }</code>
<p>Striking</p>
<code>{ "key": "Striking", "predicate":{ "all":[ "unarmed" ] }, "selector": "damage", "value": 1 }</code>
`,
Striking: `
<h3>Weapon Potency and Striking</h3>
<p>The weapon potency and striking rule elements do basically the same as the flat modifier and damage dice rule elements, but with the additional feature of properly changing the damage rolls for things like backstabber (+2 to damage with a +3 potency rune) and deadly (more dice if striking rune).</p>
<p>WeaponPotency</p>
<code>{ "key": "WeaponPotency", "selector": "{item|_id}-attack", "value": 1 }</code>
<p>Striking</p>
<code>{ "key": "Striking", "selector": "{item|_id}-damage", "value": 1 }</code>
<p>Another Example would be the Handwraps of mighty blows at +1 striking:</p>
<code>{ "key": "WeaponPotency", "predicate":{ "all":[ "unarmed" ] }, "selector": "attack", "value": 1 }</code>
<p>Striking</p>
<code>{ "key": "Striking", "predicate":{ "all":[ "unarmed" ] }, "selector": "damage", "value": 1 }</code>
`,
MultipleAttackPenalty: `
<h3>Multiple Attack Penalty</h3>
<p>The Multiple Attack Penalty rule element can change the MAP Progression of specific strikes, or the character as a whole. The two sample Rule Elements implement a flurry ranger, put them on an Effect item and they'll make you hit very often</p>
<p>MultipleAttackPenalty</p>
<code>
{
    "key":"MultipleAttackPenalty",
    "predicate": {
        "all": [
            "agile",
            "hunted-prey"
        ]
    },
    "roll-options": [
        "all"
    ],
    "selector":"attack",
    "value":-2
}
</code>
<code>
{
    "key":"MultipleAttackPenalty",
    "predicate": {
        "all": [
            "hunted-prey"
        ],
        "not": [
            "agile"
        ]
    },
    "roll-options": [
        "all"
    ],
    "selector":"attack",
    "value":-3
}
</code>
`,
LoseHitPoints: `
<h3>Lose Hit Points</h3>
<p>The LoseHitPoints rule element is for effects that remove max and current HP from an actor. The only use in the system is Drained currently, but anything else that works similarly can be automated with it. This example below removes the actor's level from hit points. The difference between this and a negative FlatModifier to hp is that this also reduces the current HP on application, effectively damaging the actor at the same time as reducing the maximum.</p>
<code>
{
    "key": "LoseHitPoints",
    "value": "@actor.level"
}
</code>
`,
AdjustStrike: `
<h3>Adjust Strike</h3>
<p>AdjustStrike allows you to modify the properties of an existing strike by adding a trait from the strike. You must provide a definition for the strikes to be modified. You can define the adjustment to hit specific weapon names/slugs, such as the example below which adds the trip trait to the wolf jaws strike when flanking.</p>
<code>
{
    "definition": {
        "all": ["weapon:wolf-jaws"]
    },
    "key": "AdjustStrike",
    "mode": "add",
    "predicate": {
        "all": ["self:flanking"]
    },
    "property": "traits",
    "value": "trip"
}
</code>
<p>Any roll option can be used in the definition, so a rule element to add sweep to all finesse weapons can be done. Note that you want to provide the full roll option in the definition. <code>weapon:trait:finesse</code> instead of just <code>finesse</code>.</p>
<p>Currently, only the <code>add</code> mode is supported. In the future other modes are planned to be added to allow for the removal or overriding of traits.</p>
`,
AdjustModifier: `
<h3>Adjust Modifier</h3>
<p>AdjustModifier allows you to take a named modifier from some source and modify it before applying it. This is useful for feats that do things like breaking stacking rules like this rule from Mountain Stance that modifies the bonus from Bracers of Armor to allow for stacking of otherwise unstackable bonuses without the need to use a separate macro.</p>
<code>
{
    "key": "AdjustModifier",
    "mode": "add",
    "relabel": "PF2E.SpecificRule.MountainStance.BracersOfArmor",
    "selector": "ac",
    "slug": "bracers-of-armor",
    "value": 4
}
</code>
<p>Note that this combines with the flat modifier on the Bracers of Armor</p>
<code>
{
    "key": "FlatModifier",
    "selector": "ac",
    "slug": "bracers-of-armor",
    "type": "item",
    "value": 1
}
</code>
<p>The AdjustModifer searches for the bonus with the same selector with the same slug and adds 4. This supports add, multiply, and override so you could double a bonus, increase it by 50%, or replace it entirely with a static number as well by changing the mode. Relabel relabels the bonus, in this case it will appear as <code>Mountain Stance w/ Bracers of Armor</code> with the relabel field referring to our system localization files, but straight text in there works to provide a custom label.</p>
`,
TokenLight:`
<h3>Token Light</h3>
<p>The TokenLight rule element can be used to add light to a token with an effect. It should contain all the needed Foundry data for a light source, so any additional data you want to pass can be done. We suggest using a sample token to get the light looking good for you then take the values from that token to fill in the needed data for the rule element. The properties of the value object correspond with the Foundry API's LightData definition. The <code>dim</code> and <code>bright</code> properties can be bracketed.</p>
<code>
{
    "key": "TokenLight",
    "value": {
        "animation": {
            "intensity": 4,
            "speed": 1,
            "type": "torch"
        },
        "bright": 20,
        "color": "#9b7337",
        "dim": 40,
        "shadows": 0.2
    }
}
</code>
`,
CriticalSpecialization: `
<p>You can define critical specialization conditions for abilities using the CriticalSpecialization rule element. Set a predicate for the conditions they get critical specialization. Barbarian Brutality for example:</p>
<code>
{
    "key": "CriticalSpecialization",
    "predicate": {
        "all": ["self:effect:rage", "weapon:melee"]
    }
}
</code>
<p>By default these pull the critical specialization from the weapon group of the weapon, but these can be override. Spike Launcher, for example, is a firearm but uses the bow critical specialization. So this same rule element is used to tell the system that if you meet the requirements for critical specialization the text should be that of the bow.</p>
<code>
{
    "alternate": true,
    "key": "CriticalSpecialization",
    "predicate": {
        "all": ["weapon:id:{item|_id}"]
    },
    "text": "PF2E.Item.Weapon.CriticalSpecialization.bow"
}
</code>
<p>This is just a call to the localization path for the bow weapon specialization. You can type any text in this to define your own.</p>
`,
SubstituteRoll: `
<h3>Substitute Roll</h3>
<p>The SubstituteRoll rule element allows you to manually set the result of a roll. This is mostly useful for assurance, but may be expanded to be how the system handles abilities like Devise a Stratagem, or Perfected Form.</p>
<p>Assurance is done in two parts, one is the SubstituteRoll and the other is an AdjustModifier to remove all non proficiency modifiers.</p>
<code>
{
    "key": "SubstituteRoll",
    "label": "PF2E.SpecificRule.SubstituteRoll.Assurance",
    "selector": "{item|flags.pf2e.rulesSelections.assurance}",
    "slug": "assurance",
    "value": 10
}
</code>
<code>
{
    "key": "AdjustModifier",
    "predicate": {
        "all": ["substitute:assurance"],
        "not": ["bonus:type:proficiency"]
    },
    "selector": "{item|flags.pf2e.rulesSelections.assurance}",
    "suppress": true
}
</code>
<p>The flags are set by the ChoiceSet rule element on the Assurance feat.</p>
`,
MarialProficiency: `
<h3>Martial Proficiency</h3>
<p>The MartialProficiency rule element creates a proficiency in a definable set of weapons. This RE from the Gunslinger class for example</p>
<code>
{
    "definition": {
        "all": [
            "weapon:category:martial"
        ],
        "any": [
            "weapon:group:firearm",
            "weapon:tag:crossbow"
        ]
    },
    "key": "MartialProficiency",
    "label": "PF2E.SpecificRule.MartialProficiency.MartialFirearmsCrossbows",
    "slug": "martial-firearms-crossbows",
    "value": 2
}
</code>
<p>This creates an expert (2) proficiency in all martial firearms and crossbows. A separate AE-Like rule can then increase the proficiency to master (3) by referencing the slug provided by this RE like so:</p>
<code>
{
    "key": "ActiveEffectLike",
    "mode": "upgrade",
    "path": "system.martial.martial-firearms-crossbows.rank",
    "value": 3
}
</code>
`,
ActiveEffectLike: `
<h3>Active Effect-Like Rule Element</h3>
<p>The ActiveEffect-Like Rule Element is incredibly powerful.  It is more flexible than regular active effects and should be preferred due to performance improvements and reliability.  The values it writes are done before many of the other steps of data preparation, making it safe to set flags that other rule elements can read from.  This rule element from the barbarian dedication feat sets a flag that the barbarian instincts can pick up in their predicates to change the damage dealt to be correct for the dedication.  By setting the flag under <code>flags.pf2e.rollOptions...</code> it can be read by predicates like those set by ToggleProperty or SetProperty.  Setting flags with this should be considered more reliable than the other rule elements since we know it comes before all other rule elements in data preparation.  Best practice is also <code>camelCaseForPropertyNames</code>.</p>
<p>Example (Barbarian Dedication flag)</p>
<code>
{
	"key": "ActiveEffectLike",
	"mode": "upgrade",
	"path": "flags.pf2e.rollOptions.all.barbarianDedication",
	"value": 1
}
</code>
<p>These flags can be stored as boolean, but javascript interprets non-zero numbers as true, and 0 as false.  So a value of <code>"@actor.level - 15"</code> would be read as true until the actor reached level 15, then turn false.  At level 16 the flag would again be read as true.</p>
<p>We can also store custom modifiers.  Each feat in the barbarian dedication has this rule element on it using the <code>add</code> mode to count the number of feats taken from the dedication.</p>
<code>
{
	"key": "ActiveEffectLike",
	"mode": "add",
	"path": "flags.pf2e.barbarianArchetype.featCount",
	"value": 1
}
</code>
<p>This is paired with a flat modifier rule element on the Barbarian Resiliency feat to add 3 HP for every taken feat.</p>
<code>
{
	"key": "FlatModifier",
	"selector": "hp",
	"value": "3 * @actor.flags.pf2e.barbarianArchetype.featCount"
}
</code>
<p>The upgrade mode can also be used to increase ranks like so</p>
<code>
{
	"key": "ActiveEffectLike",
	"mode": "upgrade",
	"path": "system.skills.rel.rank",
	"value": 1
}
</code>
<p>Lastly this rule element is able to use brackets, like this rule element from Acrobat Dedication</p>
<code>
{
	"key": "ActiveEffectLike",
	"mode": "upgrade",
	"path": "system.skills.acr.rank",
	"value": {
		"brackets": [{
			"end": 6,
			"start": 1,
			"value": 2
		}, {
			"end": 14,
			"start": 7,
			"value": 3
		}, {
			"start": 15,
			"value": 4
		}]
	}
}
</code>
<p>The brackets used in this are the same as any other rule element, however care should be taken to not rely on other rule elements or active effects to modify the bracketed value.  Character level, item level, ability scores, and other values not modified by rule elements should be safe to bracket on.  Bracketing on the rank of a skill or weapon proficiency will not be safe since these are modified by other rule elements and the order of data preparation may yield unreliable results.</p>
<p>The modes available for use are 'multiply', 'add', 'downgrade', 'upgrade', and 'override'.  It can write numbers, strings, and booleans and the value field accepts roll syntax similar to other rule elements.</p>
<p>You can add a field "priority" to control the order of application of the AE-likes. The lower the value the earlier the AE like gets applied during data preparation. without specifying they will default to 10 for multiply, 20 for add, 30 for downgrade, 40 for upgrade and 50 for override, making override usually effectively ignore all other AElikes.</p>
`,
RollOption: `
<h3>RollOption</h3>
<p>RollOption is an new extension of the AE-like rule element, but made to be easier to use for setting roll options. It should be seen as the successor to the now depreciated SetProperty RE. A RollOption RE has two mandatory fields: <code>domain</code> and <code>option</code>. These correspond to the place under <code>pf2e.flags.roll-options</code> that the property is set. The option is the thing that gets predicated on in other rule elements. So unlike ToggleProperty or the AE like example barbarian flag above you don't need to type out the entire path, or even set mod, value, etc. Only the domain and the name of the option have to be set up. Below is an alternative to the system's current rage effect RE.</p>
<p>Example RollOption (rage effect)</p>
<code>
{
    "key": "RollOption",
    "domain": "all",
    "option": "rage"
}
</code>
<p>You can use any domain or option, but not all types of rolls check all domains for options. For example an Athletics roll does not check the <code>damage-roll</code> or <code>ac</code> domains. All rolls check the <code>all</code> domain, however we want to avoid overstuffing one domain. As a general rule use <code>all</code> if unsure or if multiple types of roll will predicate off this option. Other rule elements can be told to check non-standard domains by adding the line \`"roll-options":["domain1"]". Notice that the brackets indicate an array, so you can include multiple domains to check, but you must have the brackets there.</p>
<p>You can also use RollOption to create toggleable checkboxes, as of PF2e 3.8 this takes over for the ToggleProperty rule element.</p>
<code>
{
    "domain": "ac",
    "key": "RollOption",
    "option": "nimble-dodge",
    "toggleable": true
}
</code>
`,
ChoiceSet: `
<h3>Choice Set</h3>
<p>The Choice Set Rule Element is a highly flexible Rule Element, prompting a character to make a choice from a list of options. This choice is then stored as a flag on the actor, which can be referenced in other Rule Elements, using the data path "flags.pf2e.rulesSelections..".</p>
<p>ChoiceSet</p>
<code>
{
	"key": "ChoiceSet",
    "choices": [{
        "label": "PF2E.TraitFire",
        "value": "fire"
    }, {
        "label": "PF2E.TraitWater",
        "value": "water"
    }, {
        "label": "PF2E.TraitEarth",
        "value": "earth"
    }, {
        "label": "PF2E.TraitAir",
        "value": "air"
    }]
}
</code>
<p>Another use is for the choices to be the uuids of items. In combination with Grant Item, this is used for subclass selection, and features like the Dwarf's Clan Weapon.</p>
<code>{ "key":"ChoiceSet", "prompt":"Select a clan weapon.",</code>
<p>"prompt" creates a heading on the prompt, to tell the character what choice they are making.</p>
<code>"adjustName":false,</code>
<p>"adjustName" determines whether to edit the name of the original item to include the selection made.</p>
<code>
"choices":[{
    "value":"Compendium.pf2e.equipment-srd.kJJvKm80KwWXPukV"
}, {
    "value":"Compendium.pf2e.equipment-srd.BtncTx8EfxTsHqQI"
}],
</code>
<p>The choices are defined here. A label isn't needed if a uuid is supplied, as it will default to the item's name.</p>
<code>"label":"Clan Weapon", "flag":"clanWeapon",</code>
<p>"flag" manually sets the flag's name for the rulesSelections data path. This is useful if you do not want it to use the item's name.</p>
<code>
"allowedDrops": {
    "all": [
        "item:level:0",
        "item:trait:dwarf",
        "item:type:weapon"
    ],
    "label":"level-0 dwarf weapon"}
    }
</code>
<p>"allowedDrops" allows for other items to be dropped in, and defines which items are valid to be added. The "label" here is used to indicate what items can be dragged in.</p>
<p>You can also use a <code>"RollOption"</code> field, to store the choice on the actor as a flag. This adds a prefix to the Choice, so <code>"RollOption":"prefix"</code> would create a flag of <code>"prefix:choice"</code> on the actor. This is valuable when you wish to store the choice made, for reference on other items.</p>
<p>For an example, Canny Acumen uses a Choice Set to pick either one of the three saves, or Perception, and then an AELike to upgrade our proficiency in that choice.</p>
<code>
{
	"key":"ChoiceSet",
	"choices":[{
		"label":"PF2E.SavesFortitude",
		"value":"system.saves.fortitude.rank"
	},{
		"label":"PF2E.SavesReflex",
		"value":"system.saves.reflex.rank"
	},{
		"label":"PF2E.SavesWill",
		"value":"system.saves.will.rank"
	},{
		"label":"PF2E.PerceptionLabel",
		"value":"system.attributes.perception.rank"
	}]
}
</code>
<code>
{
    "key": "ActiveEffectLike",
    "mode": "upgrade",
    "path": "{item|flags.pf2e.rulesSelections.cannyAcumen}",
    "value": {
        "brackets": [
            {
                "end": 16,
                "start": 1,
                "value": 2
            },
            {
                "start": 17,
                "value": 3
            }
        ]
    }
}
</code>
`,
GrantItem: `
<h3>Grant Item</h3>
<p>The Grant Item Rule Element allows an item to be granted to a character by another item. This occurs when the item is first added to the actor, and the Rule Element will not work if added to items already on a character.</p>
<p>The example here is the Shield Block feature for Fighter, to grant then the Shield Block general feat.</p>
<code>
{
    "key": "GrantItem",
    "uuid": "Compendium.pf2e.feats-srd.jM72TjJ965jocBV8"
}
</code>
<p>You can also use predicates to grant an item only to certain characters. This works like other predicates, and is useful for abilities and options granted depending on the character. Here, the Perpetual Infusion class feature grants a specific item depending on subclass.</p>
<code>
{
    "key": "GrantItem",
    "replaceSelf": true,
    "uuid": "Compendium.pf2e.feats-srd.7LB00jkh6JaJr3vS",
    "predicate": {
        "all": [
            "self:feature:bomber"
        ]
    }
}
</code>
<code>
{
    "key": "GrantItem",
    "replaceSelf": true,
    "uuid": "Compendium.pf2e.feats-srd.fzvIe6FwwCuIdnjX",
    "predicate": {
        "all": [
            "feature:chirurgeon"
        ]
    }
}
</code>
<p>"replaceSelf" is used here to delete the original item once the Rule Element item is granted.</p>
<p>However, due to how Grant Item works, if a character is updated to meet the predicate after the rule has been added, the item still will not be granted. You can add <code>"reevaluateOnUpdate":true</code> to cause the Rule Element to check if the character meets the prerequisites at any point, at which point the GrantItem will execute. This is useful for features that grant you additional abilities if you meet their prerequisites at any point, such as Intimidating Prowess.</p>
<p>You can also use Grant Item with Choice Set, to allow the character to choose an item from a list, and grant it to themselves. This can be seen on the Rule Elements for subclass selection, such as the Druid's Order.</p>
<code>
{
    "key": "ChoiceSet",
    "adjustName": false,
    "prompt": "Select a Druidic Order."
        "choices":[{
            "value": "Compendium.pf2e.classfeatures.POBvoXifa9HaejAg"
	    },
        {
            "value": "Compendium.pf2e.classfeatures.NdeFvIXdHwKYLiUj"
        },
        {
	        "value": "Compendium.pf2e.classfeatures.u4nlOzPj2WHkIj9l"
        },
        {
            "value": "Compendium.pf2e.classfeatures.fKTewWlYgFuhl4KA"
        },
        {
            "value": "Compendium.pf2e.classfeatures.acqqlYmti8D9QJi0"
	    },
        {
            "value": "Compendium.pf2e.classfeatures.FuUXyv2yBs7zRgqT"
        },
        {
            "value":"Compendium.pf2e.classfeatures.v0EjtiwdeMj8ykI0"
        }
    ],
    "allowedDrops": {
        "all": [
            "item:level:1",
	    "item:trait:druid",
	    "item:type:feature"
        ],
        "label":"level 1 druid class feature"
    }
}
</code>
<code>
{
    "key":"GrantItem",
    "uuid":"{item|flags.pf2e.rulesSelections.druidicOrder}"
}
</code>
`,
TempHP: `
<h3>Temp HP</h3>
<p>Just as the name implies this Rule element adds Temporary HP to a character the moment it is added. Many Temp HP Effects derive their value from player stats, the Rage Element is a prime example of how this can be done with the system. The first example is from the Aeon Stone (Pink Rhomboid).</p>
<p>TempHP</p>
<code>{ "key": "TempHP", "value": 15 }</code>
<code>{ "key": "TempHP", "value": "@actor.level + @abilities.con.mod" }</code>
`,
TokenEffectIcon: `
<h3>Token Effect Icon</h3>
<p>This will apply an effect icon to the token it is applied to.</p>
<p>TokenEffectIcon</p>
<code>{ "key": "TokenEffectIcon" }</code>
<p>By Default the Effect Icon is the Icon of the Item/feat/Effect that carries the Rule element, to change that an additional field can be used.</p>
<code>{ "key": "TokenEffectIcon","value": "systems/pf2e/icons/spells/all-is-one-one-is-all.jpg" }</code>
`,
TokenImage: `
<h3>Token Image</h3>
<p>This rule element will change the token of an actor to the boar token that is included in the system. It is useful for Wild Shape or can be customized to set expressions on PCs per effect item.</p>
<p>TokenImage</p>
<code>{ "key": "TokenImage","value": "systems/pf2e/icons/bestiary-1/boar.webp" }</code>
`,
CreatureSize: `
<h3>Creature Size</h3>
<p>This rule element can be used on a 2nd level enlarge spell effect to resize an actor and its token. Removing the item with this rule element will reset the size to the original token size again.</p>
<p>CreatureSize</p>
<code>{ "key": "CreatureSize", "value": "large" }</code>
`,
EffectTarget: `
<h3>Effect Target</h3>
<p>The effect target rule element will add a dropdown to the effect sheet, so you can make a selection - for example for a weapon. You can then use that for creating a dynamic selector to target an effect to a single weapon.</p>
<p>EffectTarget</p>
<code>{ "key": "EffectTarget", "scope": "weapon" }</code>
`,
ActorTraits: `
<h3>Adding Actor Traits</h3>
<code>{ "key": "ActorTraits", "add": ["humanoid", "human", "elf"] }</code>
`,
RollTwice: `
<h3>Roll Twice (Fortune or Misfortune)</h3>
<p>You can use the RollTwice rule element to automatically set fortune or misfortune effects.</p>
<code>
{
    "key": "RollTwice",
    "keep": "higher",
    "selector": "attack-roll"
}
</code>
<p>By default if the source is an effect item then the effect will auto expire after a single roll is made. This behavior can be overriden by setting <code>"removeAfterRoll": false</code> in the rule element.</p>
`,
AdjustDegreeOfSuccess: `
<h3>Changing Degree of Success</h3>
<p>The type can be <code>save</code>, <code>skill</code> or <code>attribute</code> (<code>attribute</code> can only take <code>perception</code> as a selector).  Adjustment has the same format as</p>
<code>interface PF2CheckDCModifiers { all?: 'one-degree-better' | 'one-degree-worse'; criticalFailure?: 'one-degree-better' | 'one-degree-worse'; failure?: 'one-degree-better' | 'one-degree-worse'; success?: 'one-degree-better' | 'one-degree-worse'; criticalSuccess?: 'one-degree-better' | 'one-degree-worse'; }</code>
<p>Example (Juggernaut Feat):</p>
<code>{"key":"AdjustDegreeOfSuccess","type":"save","selector":"fortitude","adjustment":{"success":"one-degree-better"}}</code>
<p>Example (Risky Surgery Feat):</p>
<code>{"key":"AdjustDegreeOfSuccess","type":"skill","selector":"medicine","predicate":{"all":["risky-surgery"]},"adjustment":{"success":"one-degree-better"}}</code>
`,
};

export default wiki;