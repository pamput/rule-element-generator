function capitalize(string) {
    return string.charAt(0).toUpperCase() + string.slice(1);
}
function deCapitalize(string) {
    return string.charAt(0).toLowerCase() + string.slice(1);
}

export { capitalize, deCapitalize };
