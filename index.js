async function OpenApplication(options={}) {
    let RuleElementGenerator = (await import("./modules/application.js")).default;

    new RuleElementGenerator({
        width: 750,
        height: 600,
        popOut: true,
        minimizable: true,
        resizable: false,
        id: "rule-element-generator",
        template: "modules/rule-element-generator/modules/application.html",
        title: "Rule Element Generator",
        data: options.data,
        source: options.source,
        sheet: options.sheet,
        new: options.new
    }).render(true);
}

Hooks.on("renderItemSheet", (sheet, $html) => {

    const $EditButton = $("<a class='reg-edit-rule-element' Title='Open Rule Element Generator'><i class='fas fa-magic'></i></a>")
        .click((event) => {   
            OpenApplication({
                data: JSON.parse($($(event.target).parent().parent().parent().next().children()[0]).text()),
                source: $($(event.target).parent().parent().parent().next().children()[0]).attr("name"),
                new: false
            });
        });


    const $AddButton = $('<a class="add-rule-element"><i class="fas fa-magic"></i> New Rule Element</a>')
        .off()
        .click(() => {
            OpenApplication({
                data: { key: $($(event.target).parent().children()[0]).val() },
                source: sheet,
                new: true
            });
        });

    $html
        .find("a.edit-rule-element")
        .after($EditButton);
    $html
        .find("a.add-rule-element")
        .after($AddButton);
});
